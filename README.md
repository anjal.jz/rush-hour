# Rush Hour

A simulator of the borad game Rush Hour. The "root directory" of the rushhour project is *./rushhour*

## To install the dependencies with pip
Run the following command from the root directory of the rushhour project

`pip install -r requirements.txt`  

## To run the game, make sure to use Python3 interpreter
Run the command from the root directory of the rushhour project.
`python rushHour.py`  
If the command for Python3 is *python3*, replace *python* by *python3*.

## To add more challenges
1. Add the configuration of the new challenge to file challenge/challenges_raw.txt
2. Run the following command from the root directory of the rushhou project  
`python challenge/calc_min_steps.py`
