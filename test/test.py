import pygame
import random

print(pygame.ver)

# initialize the pygame
pygame.init()

globalWidth = 1000
globalHeight = 700

# color pre-definition
white = (255, 255, 255)
black = (0, 0, 0)
red_alpha50 = (255, 0, 0, 50)
yellow = (255, 255, 0)

gameState = "pregame"

# create the screen
screen = pygame.display.set_mode((1000, 700))  # return a Surface

# title and icon
pygame.display.set_caption("前辈太空大作战")
icon = pygame.image.load("ufo.png")
pygame.display.set_icon(icon)

# background
background = pygame.image.load("background.jpg")

# player
playerImg = pygame.image.load("player.png")
playerW = playerImg.get_size()[0]  # width
playerH = playerImg.get_size()[1]  # height
playerInitX = int((globalWidth - playerW) / 2)
playerInitY = 580
playerXSpeed = 3


class Enemy:
    enemyImg = pygame.image.load("enemy.png")
    maxHP = 1
    hpconfig = {}
    hpconfig.update({"virus":1, "monster":2, "ufo":3})

    def __init__(self, **kwargs):
        rand = random.randint(1, 100)
        if rand <= 60:
            self.enemyImg = pygame.image.load("enemy.png")
            self.maxHP = self.hpconfig["virus"]
        elif rand <= 90:
            self.enemyImg = pygame.image.load("monster.png")
            self.maxHP = self.hpconfig["monster"]
        elif rand <= 100:
            self.enemyImg = pygame.image.load("ufo.png")
            self.maxHP = self.hpconfig["ufo"]


# enemy
# enemyImg = pygame.image.load("enemy.png")
# enemyW = enemyImg.get_size()[0]
# enemyH = enemyImg.get_size()[1]
# enemyInitX = int(globalWidth / 2 - enemyW)
# enemyInitY = 0 - enemyH
enemyXSpeed = 2
enemyYSpeed = 0.3

# bullet
bulletImg = pygame.image.load("bullet.png")
bulletW = bulletImg.get_size()[0]
bulletColliderW = 10
bulletH = bulletImg.get_size()[1]
bulletState = "ready"
bulletXSpeed = 0
bulletYSpeed = 15


# set initial position of player, enemy & bullet
playerX = playerInitX
playerY = playerInitY
playerX_change = 0

enemyImg = []
enemyW = []
enemyH = []
enemyInitX = []
enemyInitY = []
enemyX = []
enemyY = []
enemyX_change = []
enemyY_change = []
enemyMaxHP = []
enemyHP = []
n_init_enemies = 5
n_enemies = n_init_enemies  # number of enemy instances

for i in range(n_enemies):
    enemyImg.append(pygame.image.load("enemy.png"))
    enemyW.append(enemyImg[i].get_size()[0])
    enemyH.append(enemyImg[i].get_size()[1])
    enemyInitX.append(random.randint(0, int(globalWidth - enemyW[i])))
    enemyInitY.append(0 - enemyH[i])
    enemyX.append(enemyInitX[i])
    enemyY.append(enemyInitY[i])
    enemyX_change.append(enemyXSpeed)
    enemyY_change.append(enemyYSpeed)
    enemyMaxHP.append(1)
    enemyHP.append(enemyMaxHP[i])

bulletX = -100
bulletY = -100
bulletX_change = 0
bulletY_change = -bulletYSpeed

# score text
scoreX = 10
scoreY = 10
score_value = 0
font = pygame.font.Font("freesansbold.ttf", 24)
# font = pygame.font.SysFont("Arial", 24, True)


# game over text
def show_game_over():
    font = pygame.font.Font("freesansbold.ttf", 96)
    # font = pygame.font.SysFont("Arial", 96, True)
    gameover = font.render("GAME OVER", True, white)
    gameover_x = (globalWidth - gameover.get_width()) / 2
    gameover_y = (globalHeight - gameover.get_height()) / 2 - 100
    screen.blit(gameover, (int(gameover_x), int(gameover_y)))

    font = pygame.font.Font("freesansbold.ttf", 64)
    # font = pygame.font.SysFont("Arial", 64, True)
    finalscore = font.render("Score: " + str(score_value), True, white)
    finalscore_x = (globalWidth - finalscore.get_width()) / 2
    finalscore_y = gameover_y + gameover.get_height() + 10
    screen.blit(finalscore, (int(finalscore_x), int(finalscore_y)))

    font = pygame.font.Font("freesansbold.ttf", 32)
    # font = pygame.font.SysFont("Arial", 32, True)
    restart = font.render("Press N to restart the game", True, white)
    restart_x = (globalWidth - restart.get_width()) / 2
    restart_y = finalscore_y + finalscore.get_height() + 20
    screen.blit(restart, (int(restart_x), int(restart_y)))


def show_score():
    score = font.render("Score: " + str(score_value), True, white)
    screen.blit(score, (scoreX, scoreY))


desc_size = 32
desc_font = pygame.font.Font("freesansbold.ttf", desc_size)
virusImg = pygame.image.load("virus.png")
monsterImg = pygame.image.load("monster.png")
ufoImg = pygame.image.load("ufo.png")
virusImg = pygame.transform.scale(virusImg, (desc_size, desc_size))
monsterImg = pygame.transform.scale(monsterImg, (desc_size, desc_size))
ufoImg = pygame.transform.scale(ufoImg, (desc_size, desc_size))
virusScore = desc_font.render(": 1", True, white)
monsterScore = desc_font.render(": 2", True, white)
ufoScore = desc_font.render(": 3", True, white)


def show_enemy_desc():
    global virusImg, monsterImg, ufoImg, virusScore, monsterScore, ufoScore, desc_font
    scores_x = globalWidth - 10 - virusScore.get_width()
    icons_x = scores_x - desc_size - 10
    y = []
    for i in range(3):
        y.append(10 + i * (10 + desc_size))
    screen.blit(virusScore, (scores_x, y[0]))
    screen.blit(monsterScore, (scores_x, y[1]))
    screen.blit(ufoScore, (scores_x, y[2]))
    screen.blit(virusImg, (icons_x, y[0]))
    screen.blit(monsterImg, (icons_x, y[1]))
    screen.blit(ufoImg, (icons_x, y[2]))


def show_ctl_manual():
    global virusImg, monsterImg, ufoImg, virusScore, monsterScore, ufoScore, desc_font
    ctl_font = pygame.font.Font("freesansbold.ttf", 12)
    ctl_desc = ctl_font.render("Space:Shoot     Left/Right: Move horizontally     R: Reset spaceship", True, yellow)
    ctl_x = (globalWidth - ctl_desc.get_width()) / 2
    ctl_y = globalHeight - 10 - ctl_desc.get_height()
    screen.blit(ctl_desc, (ctl_x, ctl_y))


def player(x, y):
    # Draw a source surface "player" onto surface "screen"
    screen.blit(playerImg, (x, y))  # return a Rect


def enemy(i, x, y):
    # Draw a source surface "enemy" onto surface "screen"
    screen.blit(enemyImg[i], (x, y))  # return a Rect


def bullet(x, y):
    # Draw a source surface "enemy" onto surface "screen"
    screen.blit(bulletImg, (x, y))  # return a Rect


def fire_bullet(x, y):
    global bulletState
    bulletState = "fired"
    # Draw a source surface "enemy" onto surface "screen"
    screen.blit(bulletImg, (x, y))  # return a Rect\


def is_collision(enemy_x, enemy_y, bullet_x, bullet_y):
    max_x = enemy_x + enemyW[i] if enemy_x + enemyW[i] > bullet_x + bulletColliderW else bullet_x + bulletColliderW
    max_y = enemy_y + enemyH[i] if enemy_y + enemyH[i] > bullet_y + bulletH else bullet_y + bulletH
    min_x = enemy_x if enemy_x < bullet_x else bullet_x
    min_y = enemy_y if enemy_y < bullet_y else bullet_y
    if max_x - min_x <= enemyW[i] + bulletColliderW and max_y - min_y <= enemyH[i] + bulletH:
        return True
    return False


def on_hit(i):
    global score_value, enemyX, enemyY, enemyHP
    # score_value += 1
    # enemyX[i] = random.randint(0, int(globalWidth / 2 - enemyW[i]))
    # enemyY[i] = enemyInitY[i]
    enemyHP[i] -= 1


diff_since_last_increase = 0


def on_eliminate(i):
    global score_value, enemyX, enemyY, enemyMaxHP, diff_since_last_increase
    score_value += enemyMaxHP[i]
    enemyX[i] = random.randint(0, int(globalWidth - enemyW[i]))
    enemyY[i] = enemyInitY[i]
    enemy_respawn(i)
    diff_since_last_increase += enemyMaxHP[i]
    if diff_since_last_increase >= 25:
        difficulty_increase()
        diff_since_last_increase = diff_since_last_increase - 25


def enemy_respawn(i):
    global enemyImg, enemyMaxHP, enemyHP
    new_enemy = Enemy()
    enemyImg[i] = new_enemy.enemyImg
    enemyMaxHP[i] = new_enemy.maxHP
    enemyHP[i] = enemyMaxHP[i]


def difficulty_increase():
    global enemyImg, enemyW, enemyY, enemyInitX, enemyInitY, enemyX, enemyY, enemyMaxHP, enemyHP, n_enemies
    i = n_enemies
    enemyImg.append(pygame.image.load("enemy.png"))
    enemyW.append(enemyImg[i].get_size()[0])
    enemyH.append(enemyImg[i].get_size()[1])
    enemyInitX.append(random.randint(0, int(globalWidth - enemyW[i])))
    enemyInitY.append(0 - enemyH[i])
    enemyX.append(enemyInitX[i])
    enemyY.append(enemyInitY[i])
    enemyX_change.append(enemyXSpeed)
    enemyY_change.append(enemyYSpeed)
    enemyMaxHP.append(1)
    enemyHP.append(enemyMaxHP[i])
    n_enemies += 1


def reset_player_position():
    global playerX
    global playerY
    playerX = playerInitX
    playerY = playerInitY


def reset_bullet():
    global bulletX, bulletY, bulletState
    bulletX = -100
    bulletY = -100
    bulletState = "ready"


def on_game_over():
    global gameState
    gameState = "gameover"
    global enemyX, enemyY
    for i in range(n_enemies):
        enemyX[i] = enemyInitX[i]
        enemyY[i] = enemyInitY[i]


def on_restart():
    global gameState, score_value, enemyW, enemyY, enemyInitX, enemyInitY, enemyX, enemyY, enemyMaxHP, enemyHP, n_enemies
    gameState = "gaming"
    score_value = 0
    for i in range(5, n_enemies):
        enemyImg.pop()
        enemyW.pop()
        enemyH.pop()
        enemyInitX.pop()
        enemyInitY.pop()
        enemyX.pop()
        enemyY.pop()
        enemyX_change.pop()
        enemyY_change.pop()
        enemyMaxHP.pop()
        enemyHP.pop()
        n_enemies = n_init_enemies

gameState = "gaming"

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = -playerXSpeed  # the player starts to move towards left
            elif event.key == pygame.K_RIGHT:
                playerX_change = playerXSpeed  # the player starts to move towards right
            elif event.key == pygame.K_SPACE:
                print("space pressed")
                # if bulletState == "ready":
                #     bulletX = int(playerX + (playerW - bulletW) / 2)
                #     bulletY = int(playerY)
                #     fire_bullet(bulletX, bulletY)  # the player fires a bullet from the top of it
            elif event.key == pygame.K_n:
                if gameState == "gameover":
                    on_restart()
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                pressed = pygame.key.get_pressed()
                if not pressed[pygame.K_LEFT] and not pressed[pygame.K_RIGHT]:
                    playerX_change = 0
                    # playerY_change = 0
            elif event.key == pygame.K_r:  # 按r恢复原位
                reset_player_position()

    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_SPACE]:
        if bulletState == "ready":
            bulletX = int(playerX + (playerW - bulletW) / 2)
            bulletY = int(playerY)
            fire_bullet(bulletX, bulletY)  # the player fires a bullet from the top of it

    # Fill the background with white
    screen.fill(black)

    # Blit the background image
    screen.blit(background, (0, 0), (0, 0, 1000, 700))

    # checking boundaries of the player
    if playerX_change != 0:
        playerX += playerX_change
        if playerX < 0:
            playerX = 0
        elif playerX > globalWidth - playerW:
            playerX = globalWidth - playerW

    # enemy movement
    if gameState == "gaming":
        for i in range(n_enemies):
            if enemyX_change[i] != 0:
                enemyX[i] += enemyX_change[i]
                if enemyX[i] < 0:
                    enemyX[i] = 0
                    enemyX_change[i] = -enemyX_change[i]
                elif enemyX[i] > globalWidth - enemyW[i]:
                    enemyX[i] = globalWidth - enemyW[i]
                    enemyX_change[i] = -enemyX_change[i]
            if enemyY_change[i] != 0:
                enemyY[i] += enemyY_change[i]

    # bullet movement
    if bulletState == "fired":
        bulletY += bulletY_change
        if bulletY < -bulletH:  # reached out of the top of the window
            bulletY = int(playerY - bulletH)
            bulletState = "ready"

    # collision detection
    if gameState == "gaming":
        for i in range(n_enemies):
            if is_collision(enemyX[i], enemyY[i], bulletX, bulletY):
                reset_bullet()
                on_hit(i)
                if enemyHP[i] <= 0:
                    on_eliminate(i)
                break

    # draw
    for i in range(n_enemies):
        enemy(i, int(enemyX[i]), int(enemyY[i]))
        if enemyY[i] + enemyH[i] > playerY:
            on_game_over()
    if gameState == "gameover":
        show_game_over()
    if bulletState == "fired":
        bullet(int(bulletX), int(bulletY))
    player(int(playerX), playerY)  # display player on top of bullet
    show_score()
    show_enemy_desc()
    show_ctl_manual()

    # update
    pygame.display.update()

    #
    # # Draw a solid blue circle in the center
    #
    # pygame.draw.circle(screen, (0, 0, 255), (250, 250), 75)
    #
    # # Flip the display
    #
    # pygame.display.flip()

input("please input any key to exit!")