import pygame
import sys
import os
import json
directory = os.path.split(os.path.realpath(__file__))[0]
sys.path.append("{}/..".format(directory))
import search
import time
from common import Common
from car import Car



outer_edge_width = 30
max_car_length = 3
target_car = ""
# cars_instances = {}


def init_cars(config):
    global target_car
    cars_instances = {}
    for k, v in config.items():
        print(k)
        carImg = pygame.image.load("{}/../img/car_{}.png".format(directory, k))  # returns a surface
        car = Car(k, int(v["length"]), Common.translate_j_to_x(v["j"]), Common.translate_i_to_y(v["i"]), v["direction"], carImg,
                  board_offset=outer_edge_width)
        cars_instances[k] = car
    target_car = cars_instances[next(iter(cars_instances))]
    return cars_instances


def init_lot_state() -> list():
    lot_state = [["." for i in range(6)] for j in range(6)]
    for _ in range(max_car_length):
        lot_state[2].append(".")  # Give additional space for the row where the exit is located
    return lot_state


def get_current_lot_state(cars) -> list:
    current_lot_state = init_lot_state()
    for car in cars.values():
        j = Common.translate_x_to_j(car.collider.x) - 1
        i = Common.translate_y_to_i(car.collider.y) - 1
        if car.direction == "H":
            for k in range(car.length):
                current_lot_state[i][j + k] = car.alias
        elif car.direction == "V":
            for k in range(car.length):
                current_lot_state[i + k][j] = car.alias
    return current_lot_state


def search_for_best_solution_for_all_challenges():
    challenges = dict()
    with open((directory + "/challenges_raw.txt"), "rt") as f:
        with open((directory + "/min_steps.json"), "wt") as f2:
            lines = f.readlines()
            min_steps = dict()
            for i in range(len(lines)):
                if lines[i][0] == "#":
                    temp = lines[i].split()
                    challenge_alias = temp[1]
                    # challenges[challenge_alias] = {}
                    difficulty = lines[i + 1]
                    temp = lines[i + 2].split()
                    n_cars = int(temp[0])
                    # n_moves_sol = int(temp[1])
                    cars = {}
                    for j in range(1, n_cars + 1):
                        attr = lines[i + 2 + j].split()
                        car_alias = attr[0]
                        cars[car_alias] = {}
                        cars[car_alias]["length"] = int(attr[1])
                        cars[car_alias]["j"] = int(attr[2])
                        cars[car_alias]["i"] = int(attr[3])
                        cars[car_alias]["direction"] = attr[4]
                    # challenges[challenge_alias]["config"] = cars
                    print(f"challenge {challenge_alias}")
                    cars_instances = init_cars(cars)

                    # bfs
                    solution, n_moves_sol, n_nodes_visited = search.bfs(get_current_lot_state(cars_instances), cars_instances, target_car)
                    min_steps[challenge_alias] = n_moves_sol
                    print(n_moves_sol)
            f2.write(json.dumps(min_steps, sort_keys=True, indent=4, separators=(',', ':')))


def search_for_best_solution_for_unsolved_challenges():
    challenges = dict()
    lines = ""
    min_steps = dict()
    with open((directory + "/min_steps.json"), "rt") as f1:
        min_steps = json.loads(f1.read())
    with open((directory + "/challenges_raw.txt"), "rt") as f:
        lines = f.readlines()
    for i in range(len(lines)):
        if lines[i][0] == "#":
            temp = lines[i].split()
            challenge_alias = temp[1]
            # challenges[challenge_alias] = {}
            if challenge_alias in min_steps.keys():
                continue
            difficulty = lines[i + 1]
            temp = lines[i + 2].split()
            n_cars = int(temp[0])
            # n_moves_sol = int(temp[1])
            cars = {}
            for j in range(1, n_cars + 1):
                attr = lines[i + 2 + j].split()
                car_alias = attr[0]
                cars[car_alias] = {}
                cars[car_alias]["length"] = int(attr[1])
                cars[car_alias]["j"] = int(attr[2])
                cars[car_alias]["i"] = int(attr[3])
                cars[car_alias]["direction"] = attr[4]
            # challenges[challenge_alias]["config"] = cars
            print(f"challenge {challenge_alias}")
            cars_instances = init_cars(cars)
            solution, n_moves_sol, _ = search.bfs(get_current_lot_state(cars_instances), cars_instances, target_car)
            # challenges[challenge_alias]["n_moves_sol"] = n_moves_sol
            min_steps[challenge_alias] = n_moves_sol
            print(n_moves_sol)
            # f2.write(f"{challenge_alias} {n_moves_sol}\n")
    with open((directory + "/min_steps.json"), "wt") as f2:
        f2.write(json.dumps(min_steps, sort_keys=True, indent=4, separators=(',', ':')))


def different_algorithm_search_for_best_solution_for_all_challenges():
    challenges = dict()
    min_steps = dict()
    with open((directory + "/challenges_raw.txt"), "rt") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            if lines[i][0] == "#":
                temp = lines[i].split()
                challenge_alias = temp[1]
                # challenges[challenge_alias] = {}
                difficulty = lines[i + 1]
                temp = lines[i + 2].split()
                n_cars = int(temp[0])
                # n_moves_sol = int(temp[1])
                cars = {}
                for j in range(1, n_cars + 1):
                    attr = lines[i + 2 + j].split()
                    car_alias = attr[0]
                    cars[car_alias] = {}
                    cars[car_alias]["length"] = int(attr[1])
                    cars[car_alias]["j"] = int(attr[2])
                    cars[car_alias]["i"] = int(attr[3])
                    cars[car_alias]["direction"] = attr[4]
                # challenges[challenge_alias]["config"] = cars
                print(f"challenge {challenge_alias}")
                cars_instances = init_cars(cars)

                # bfs
                start_time = time.time()
                solution, n_moves_sol, n_nodes_visited = search.bfs(get_current_lot_state(cars_instances), cars_instances, target_car)
                end_time = time.time()
                search_time_bfs = end_time - start_time
                min_steps[challenge_alias] = dict()
                min_steps[challenge_alias]["bfs"] = dict()
                min_steps[challenge_alias]["bfs"]["search_time"] = search_time_bfs
                min_steps[challenge_alias]["bfs"]["min_steps"] = n_moves_sol
                min_steps[challenge_alias]["bfs"]["num_nodes_visited"] = n_nodes_visited
                print(n_moves_sol)

                # A*
                start_time = time.time()
                solution, n_moves_sol, n_nodes_visited = search.a_star_search(get_current_lot_state(cars_instances),
                                                                    cars_instances, target_car)
                end_time = time.time()
                search_time_astar = end_time - start_time
                min_steps[challenge_alias]["a_star"] = dict()
                min_steps[challenge_alias]["a_star"]["search_time"] = search_time_astar
                # challenges[challenge_alias]["n_moves_sol"] = n_moves_sol
                min_steps[challenge_alias]["a_star"]["min_steps"] = n_moves_sol
                min_steps[challenge_alias]["a_star"]["num_nodes_visited"] = n_nodes_visited
                print(n_moves_sol)
    with open((directory + "/algorithm_data_a_star_heap.json"), "wt") as f2:
        f2.write(json.dumps(min_steps, sort_keys=True, indent=4, separators=(',', ':')))


if __name__ == "__main__":
    if "-all" in sys.argv:
        search_for_best_solution_for_all_challenges()
    elif "-algorithms" in sys.argv:
        different_algorithm_search_for_best_solution_for_all_challenges()
    else:
        search_for_best_solution_for_unsolved_challenges()
