//Format
//# (challenge number)
//(difficulty)
//(number of cars in this challenge)
//(CarAlias) (length) (j) (i) (direction)

# 7
beginner
9
X 2 2 3 H
A 2 2 1 V
B 2 3 1 H
C 2 5 1 V
D 2 6 1 V
E 2 4 2 V
F 2 6 3 V
H 2 4 5 V
I 2 3 4 H

# 8
beginner
14
X 2 1 3 H
A 2 4 1 H
B 2 3 2 H
C 2 5 2 V
D 2 3 3 V
E 2 4 3 V
F 2 1 4 H
G 2 5 4 H
H 2 1 5 H
I 2 3 5 V
K 2 1 6 H
O 3 6 1 V
P 3 4 5 H
Q 3 4 6 H

# 801
beginner
9
X 2 1 3 H
A 2 1 1 V
B 2 2 4 V
C 2 4 2 H
D 2 5 3 V
O 3 6 1 V
P 3 3 5 H
Q 3 3 6 H
R 3 3 1 V

# 0
beginner
1
X 2 1 3 H

# 302
intermediate
13
Y 3 2 3 H
A 2 2 1 V
B 2 5 1 H
C 2 4 2 H
D 2 5 3 V
E 2 1 4 H
F 2 3 4 V
G 2 4 4 V
H 2 5 5 H
I 2 2 6 H
J 2 4 6 H
O 3 1 1 V
P 3 6 2 V

# 325
advanced
13
X 2 2 3 H
A 2 1 1 H
B 2 3 1 V
C 2 5 1 H
D 2 1 2 H
E 2 5 3 V
F 2 2 5 V
G 2 4 5 V
H 2 5 5 H
I 2 5 6 H
O 3 6 2 V
P 3 1 3 V
Q 3 2 4 H

# 321
expert
14
Y 3 1 3 H
A 2 1 1 H
B 2 4 1 H
C 2 1 2 H
D 2 3 2 H
E 2 5 2 V
F 2 4 3 V
G 2 5 4 H
H 2 2 5 H
I 2 4 5 H
J 2 5 6 H
O 3 6 1 V
P 3 1 4 V
Q 3 2 6 H

# 332
grand_master
10
X 2 4 3 H
A 2 4 1 V
B 2 1 2 V
C 2 2 2 H
D 2 1 4 H
O 3 1 1 H
P 3 6 1 V
Q 3 3 3 V
R 3 4 5 H
Y 3 3 6 H

# 3
beginner
6
X 2 2 3 H
A 2 2 4 H
B 2 2 5 V
C 2 3 6 H
O 3 4 3 V
P 3 6 4 V

# 303
intermediate
10
X 2 1 3 H
A 2 4 2 H
B 2 4 3 V
C 2 5 4 H
D 2 5 5 V
O 3 3 1 H
P 3 6 1 V
Q 3 3 2 V
R 3 2 5 H
Y 3 2 6 H

# 304
intermediate
13
X 2 1 3 H
A 2 6 1 V
B 2 3 2 H
C 2 3 3 V
D 2 4 4 H
E 2 2 5 V
F 2 3 6 H
G 2 5 6 H
O 3 2 1 H
P 3 5 1 V
Q 3 6 3 V
R 3 1 4 V
Y 3 3 5 H

# 333
grand_master
13
Y 3 1 3 H
A 2 3 1 V
B 2 4 1 H
C 2 6 1 V
D 2 4 3 V
E 2 2 4 H
F 2 2 5 V
G 2 4 5 H
H 2 3 6 H
I 2 5 6 H
O 3 5 2 V
P 3 6 3 V
Q 3 1 4 V

# 340
grand_master
13
X 2 1 3 H
A 2 1 1 H
B 2 3 1 V
C 2 5 1 H
D 2 5 2 H
E 2 3 3 V
F 2 6 3 V
G 2 4 4 H
H 2 2 5 H
I 2 4 5 V
J 2 6 5 V
K 2 2 6 H
O 3 1 4 V

# 29
advanced
12
X 2 1 3 H
A 2 3 2 V
B 2 6 3 V
C 2 1 4 V
D 2 2 4 H
E 2 4 4 H
F 2 2 5 H
G 2 4 5 V
H 2 6 5 V
O 3 1 1 H
P 3 5 1 V
R 3 1 6 H

# 19
intermediate
8
X 2 3 3 H
A 2 3 1 V
B 2 4 1 H
D 2 2 3 V
E 2 3 4 H
F 2 5 4 V
J 2 5 2 V
O 3 2 5 H