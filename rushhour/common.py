import pygame
import os
directory = os.path.split(os.path.realpath(__file__))[0]
import json

class Common:
    @staticmethod
    def translate_index_to_coordinate(j, i):
        return int(80 * (j - 1)), int(80 * (i - 1))

    @staticmethod
    def translate_x_to_j(x):  # i ranges from 1 to 6
        return int(x / 80 + 1)

    @staticmethod
    def translate_y_to_i(y):  # j ranges from 1 to 6
        return int(y / 80 + 1)

    @staticmethod
    def translate_coordinate_to_index(x, y):
        return int(80 * (x - 1)), int(80 * (y - 1))

    @staticmethod
    def translate_j_to_x(j):
        return int(80 * (j - 1))

    @staticmethod
    def translate_i_to_y(i):
        return int(80 * (i - 1))

    @staticmethod
    def load_challenges():
        challenges = dict()
        lines = ""
        min_steps = dict()
        with open(f"{directory}/challenge/challenges_raw.txt", "rt") as f:
            lines = f.readlines()
        with open(f"{directory}/challenge/min_steps.json", "rt") as f1:
            min_steps = json.loads(f1.read())
        for i in range(len(lines)):
            if lines[i][0] == "#":
                temp = lines[i].split()
                challenge_alias = temp[1]
                challenges[challenge_alias] = {}
                difficulty = lines[i + 1].strip()
                temp = lines[i + 2].split()
                n_cars = int(temp[0])
                # n_moves_sol = int(temp[1])
                cars = {}
                for j in range(1, n_cars + 1):
                    attr = lines[i + 2 + j].split()
                    car_alias = attr[0]
                    cars[car_alias] = {}
                    cars[car_alias]["length"] = int(attr[1])
                    cars[car_alias]["j"] = int(attr[2])
                    cars[car_alias]["i"] = int(attr[3])
                    cars[car_alias]["direction"] = attr[4]
                challenges[challenge_alias]["difficulty"] = difficulty
                challenges[challenge_alias]["config"] = cars
                challenges[challenge_alias]["n_moves_sol"] = min_steps[challenge_alias]
        # # sort the challenges by int(alias)
        challenges = {k: v for k, v in sorted(challenges.items(), key=lambda item: int(item[0]))}
        return challenges


class RHButton(object):
    name = "unnamed"
    mouse_down = False
    is_active = False

    def __init__(self, x, y, w, h, ic, ac, action=None, param=None):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.rect = pygame.Rect((self.x, self.y, self.w, self.h))
        self.ic = ic
        self.ac = ac
        self.action = action
        self.param = param

    def trigger(self, param=None):
        if self.action is not None:
            if param is not None:
                self.action(param)
            elif self.param is not None:
                self.action(self.param)
            else:
                self.action()
        else:
            raise ValueError("undefined function being called")

    def get_rect(self):
        return self.rect

    def on_mouse_down(self):
        self.mouse_down = True

    def on_mouse_hover(self):
        self.is_active = True

    def on_mouse_hover_out(self):
        self.is_active = False

    def on_mouse_up(self, param=None):
        if self.mouse_down :
            self.trigger(param)
            # print("RHButton.on_mouse_up")
        self.mouse_down = False

    def blit(self, surface):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if self.get_rect().collidepoint((mouse_x, mouse_y)):
            self.on_mouse_hover()
        else:
            self.on_mouse_hover_out()
        if self.is_active:
            back = pygame.draw.rect(surface, self.ac, self.get_rect())
        else:
            back = pygame.draw.rect(surface, self.ic, self.get_rect())

    def set_name(self, name):
        self.name = name

    @classmethod
    def text_objects(cls, text, font):
        text_surface = font.render(text, True, (0, 0, 0))
        return text_surface, text_surface.get_rect()


class TextButton(RHButton):
    def __init__(self, x, y, w, h, ic, ac, msg="", action=None, param=None):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.rect = pygame.Rect((self.x, self.y, self.w, self.h))
        self.ic = ic
        self.ac = ac
        self.msg = msg
        self.action = action
        self.param = param

    def blit(self, surface):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if self.get_rect().collidepoint((mouse_x, mouse_y)):
            self.on_mouse_hover()
        else:
            self.on_mouse_hover_out()
        if self.is_active:
            back = pygame.draw.rect(surface, self.ac, self.get_rect())
        else:
            back = pygame.draw.rect(surface, self.ic, self.get_rect())
        small_text = pygame.font.SysFont("comicsansms", 20)
        text_surf, text_rect = self.text_objects(self.msg, small_text)
        text_rect.center = ((self.x + (self.w / 2)), (self.y + (self.h / 2)))
        surface.blit(text_surf, text_rect)


class IconButton(RHButton):
    def __init__(self, x, y, image1=None, image2=None, action1to2=None, action2to1=None, param=None):
        self.x = x
        self.y = y
        self.img1 = image1
        self.img2 = image2
        self.img_index = 1
        self.action1to2 = action1to2
        self.action2to1 = action2to1
        self.param = param

    def get_curr_img(self):
        img_key = f"img{self.img_index}"
        return self.__dict__[img_key]

    def blit(self, surface):
        # mouse_x, mouse_y = pygame.mouse.get_pos()
        # if self.get_rect().collidepoint((mouse_x, mouse_y)):
        #     self.on_mouse_hover()
        # else:
        #     self.on_mouse_hover_out()
        surface.blit(self.get_curr_img(), (self.x, self.y))

    def get_rect(self):
        curr_img = self.get_curr_img()
        return curr_img.get_rect(left = self.x, top = self.y)

    def trigger(self, param=None):
        if self.img_index == 1:
            # print("triggering button from 1 to 2")
            self.img_index = 2
            if self.action1to2 is not None:
                if param is not None:
                    self.action1to2(param)
                elif self.param is not None:
                    self.action1to2(self.param)
                else:
                    self.action1to2()
            else:
                raise ValueError("undefined function being called")

        else:
            self.img_index = 1
            if self.action2to1 is not None:
                if param is not None:
                    self.action2to1(param)
                elif self.param is not None:
                    self.action2to1(self.param)
                else:
                    self.action2to1()
            else:
                raise ValueError("undefined function being called")