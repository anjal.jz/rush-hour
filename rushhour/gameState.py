from enum import Enum, unique

@unique
class GameState(Enum):
    MAIN = 1
    CHOOSE_CHALLENGE = 2
    CHALLENGING = 3
    CHALLENGE_COMPLETE = 4


class GameSwitches:
    auto_moving = False
    auto_move_stopping = False

    def reset(self):
        self.auto_moving = False
        self.auto_move_stopping = False
