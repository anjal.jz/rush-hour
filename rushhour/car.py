import pygame
import os


class Car:
    x = 2
    y = 4
    direction = "H"  # horizontal
    image = pygame.image.load((os.path.split(os.path.realpath(__file__))[0] + "/img/car1.png"))
    collider = image.get_rect()

    def __init__(self, alias, length, x, y, direction, image, **kwargs):
        for k, v in kwargs.items():
            # print(type(k))
            self.__dict__[k] = v

        self.alias = alias
        self.length = length
        self.x = x
        self.y = y
        self.j = int(x / 80 + 1) - 1  # It is for bfs to compute, so it starts from 0 给bfs计算，所以这里用0起始
        self.i = int(y / 80 + 1) - 1  # It is for bfs to compute, so it starts from 0 给bfs计算，所以这里用0起始
        self.direction = direction
        self.image = image
        if self.length == 2:
            self.image = pygame.transform.scale(self.image, (80, 160))
        elif self.length == 3:
            self.image = pygame.transform.scale(self.image, (80, 240))
        if direction == "H":
            self.image = pygame.transform.rotate(self.image, -90)
        self.collider = self.image.get_rect()
        self.collider.topleft = (x, y)

    def print_dict(self):
        for a, _ in self.__dict__.items():
            print("self.__dict__    " + a)
        for a, _ in Car.__dict__.items():
            print("Car.__dict__    " + a)
        print(Car.x)
        print(self.x)
        print(self.y)


# car = Car(cat="meow", x=1)
# print(car.cat)
# car.print_dict()
