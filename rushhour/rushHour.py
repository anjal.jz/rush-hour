import json

import pygame
import sys
import math
import time
import pygame_functions
import pprint
import math

sys.path.append(".")
import os
directory = os.path.split(os.path.realpath(__file__))[0]
from rushhour.car import Car
from rushhour.gameState import GameState
from rushhour.gameState import GameSwitches
from rushhour.common import *
import rushhour.search
# from pygame_textinput import TextInput

print(pygame.ver)

# initialize the pygame
pygame.init()

globalWidth = 1000
globalHeight = 700

# colors
white = (255, 255, 255)
black = (0, 0, 0)
red = (200, 0, 0)
green = (0, 200, 0)
yellow = (200, 200, 0)
bright_red = (255, 0, 0)
bright_green = (0, 255, 0)
bright_yellow = (255, 255, 0)

gameState = GameState.MAIN
game_switches = GameSwitches()


# create the screen
screen = pygame.display.set_mode((1000, 700))  # return a Surface
pygame_functions.screen = screen
pygame_functions.background = pygame_functions.Background()
pygame_functions.background.surface = screen.copy()

# title and icon
pygame.display.set_caption("Rush Hour Emulator")
icon = pygame.image.load(f"{directory}/img/rush-hour.jpg")
pygame.display.set_icon(icon)

# Background
background = pygame.image.load(f"{directory}/img/background.png").convert()
screen.blit(background, (0, 0))

# music
pygame.mixer.init()
pygame.mixer.music.load(f"{directory}/audio/bgm.mp3")
pygame.mixer.music.play(-1, 0)
pygame.mixer.music.set_volume(0.05)

# sounds
sound_hooray = pygame.mixer.Sound(f"{directory}/audio/children-hooray-joy-shout.wav")
sound_hooray.set_volume(0.2)

# clock
fpsClock = pygame.time.Clock()

# global widgets
buttons_global = list()

# challenging buttons
buttons_challenging = list()

# challenge_complete buttons
buttons_challenge_complete = list()

# default username
username = "player A"

# data
user_data = {}

# icons in game
icon_difficulty_beginner = pygame.image.load(f"{directory}/img/difficulty_beginner.png")
icon_difficulty_intermediate = pygame.image.load(f"{directory}/img/difficulty_intermediate.png")
icon_difficulty_advanced = pygame.image.load(f"{directory}/img/difficulty_advanced.png")
icon_difficulty_expert = pygame.image.load(f"{directory}/img/difficulty_expert.png")
icon_difficulty_grand_master = pygame.image.load(f"{directory}/img/difficulty_grand_master.png")


def blit_background():
    screen.blit(background, (0, 0))
    # show_global_buttons()


def init_user_data(username):
    global user_data
    user_data[username] = dict()
    # user_data[username]["nick"] = username
    user_data[username]["challenges_record"] = {k: dict() for k in challenges.keys()}
    for k, v in user_data[username]["challenges_record"].items():
        v["challenge_alias"] = k
        v["best_score"] = 0
        v["min_moves_taken"] = "N/A"


def load_user_data():
    global user_data
    if os.path.exists(directory + "/data/userdata.json"):
        with open((directory + "/data/userdata.json"), "rt") as f:
            datastr = f.read()
            if datastr != "":
                user_data = json.loads(datastr)
        if len(user_data) == 0:
            init_user_data(username)
    else:
        with open((directory + "/data/userdata.json"), "w+t") as f:  # if data/userdata.json does not exist, create one
            init_user_data(username)
            f.write(json.dumps(user_data, sort_keys=False, indent=4, separators=(',', ':')))


def write_user_data():
    with open((directory + "/data/userdata.json"), "wt") as f:
        f.write(json.dumps(user_data, sort_keys=False, indent=4, separators=(',', ':')))


_circle_cache = {}
def _circlepoints(r):
    r = int(round(r))
    if r in _circle_cache:
        return _circle_cache[r]
    x, y, e = r, 0, 1 - r
    _circle_cache[r] = points = []
    while x >= y:
        points.append((x, y))
        y += 1
        if e < 0:
            e += 2 * y - 1
        else:
            x -= 1
            e += 2 * (y - x) - 1
    points += [(y, x) for x, y in points if x > y]
    points += [(-x, y) for x, y in points if x]
    points += [(x, -y) for x, y in points if y]
    points.sort()
    return points


def render(text, font, gfcolor=pygame.Color('dodgerblue'), ocolor=(255, 255, 255), opx=2):
    textsurface = font.render(text, True, gfcolor).convert_alpha()
    w = textsurface.get_width() + 2 * opx
    h = font.get_height()

    osurf = pygame.Surface((w, h + 2 * opx)).convert_alpha()
    osurf.fill((0, 0, 0, 0))

    surf = osurf.copy()

    osurf.blit(font.render(text, True, ocolor).convert_alpha(), (0, 0))

    for dx, dy in _circlepoints(opx):
        surf.blit(osurf, (dx + opx, dy + opx))

    surf.blit(textsurface, (opx, opx))
    return surf


def text_objects(text, font):
    text_surface = font.render(text, True, black)
    return text_surface, text_surface.get_rect()


def button(msg, x, y, w, h, ic, ac, action=None, param=None):
    """
    ic: inactive color
    ac: active color
    """
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    # print(click)
    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(screen, ac, (x, y, w, h))
        if click[0] == 1 and action is not None:
            if param is not None:
                action(param)
            else:
                action()
            return
    else:
        pygame.draw.rect(screen, ic, (x, y, w, h))

    small_text = pygame.font.SysFont("comicsansms", 20)
    text_surf, text_rect = text_objects(msg, small_text)
    text_rect.center = ((x + (w / 2)), (y + (h / 2)))
    screen.blit(text_surf, text_rect)


# def register_challenging_button(x, y, w, h):
#     global buttons_challenging
#     buttons_challenging.append(pygame.Rect(x, y, w, h))
#
#
# def clear_register_challenging_button(x, y, w, h):
#     global buttons_challenging
#     buttons_challenging.clear()


names = locals()
def challenge_button(msg, x, y, w, h, ic, ac, name="", action=None, param=None):
    """
    ic: inactive color
    ac: active color
    """
    global names
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    # print(click)
    names[name] = pygame.Rect(x, y, w, h)
    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(screen, ac, names[name])
        if click[0] == 1 and action is not None:
            if param is not None:
                action(name, param)
            else:
                action(name)
    else:
        pygame.draw.rect(screen, ic, (x, y, w, h))

    small_text = pygame.font.SysFont("comicsansms", 20)
    text_surf, text_rect = text_objects(msg, small_text)
    text_rect.center = ((x + (w / 2)), (y + (h / 2)))
    screen.blit(text_surf, text_rect)


def register_global_buttons():
    global buttons_global
    # music button
    button_img_music = pygame.image.load(f"{directory}/img/icon_music.png")
    button_img_music_disabled = pygame.image.load(f"{directory}/img/icon_music_disabled.png")
    button_img_music = pygame.transform.scale(button_img_music, (50, 50))
    button_img_music_disabled = pygame.transform.scale(button_img_music_disabled, (50, 50))
    button_music = IconButton(20, globalHeight - 50 - 20, button_img_music, button_img_music_disabled, on_disable_music, on_enable_music)
    buttons_global.append(button_music)


def show_global_buttons():
    for button in buttons_global:
        button.blit(screen)


def check_global_buttons(event):
    # print(f"called check_global_buttons with event type{event.type}")
    if hasattr(event, "pos"):
        mouse_x, mouse_y = event.pos
    else:
        mouse_x, mouse_y = pygame.mouse.get_pos()
    button_to_operate = None
    for button in buttons_global:
        # print(f"button postion {button.get_rect()}, mouse position {mouse_x}, {mouse_y}")
        if button.get_rect().collidepoint(mouse_x, mouse_y):
            button_to_operate = button
            break
    else:
        return
    if event.type == pygame.MOUSEBUTTONDOWN:
        button_to_operate.on_mouse_down()
    elif event.type == pygame.MOUSEBUTTONUP:
        button_to_operate.on_mouse_up()


def on_disable_music():
    print("on_disable_music")
    pygame.mixer.music.stop()


def on_enable_music():
    print("on_enable_music")
    pygame.mixer.music.play(-1, 0)


def on_start_challenge():
    global gameState, n_moves_taken, moves_history, score, selected_challenge_alias, dragging_car
    init_challenge(selected_challenge_alias)
    gameState = GameState.CHALLENGING
    register_challenging_buttons()
    # screen.fill(black)
    blit_background()
    game_switches.reset()
    reset_challenge()


def register_challenging_buttons():
    global buttons_challenging
    # hint button
    button_hint = TextButton(550, 240, 100, 50, green, bright_green, "Hint", on_hint)
    button_hint.set_name("button_hint")
    buttons_challenging.append(button_hint)
    # return button
    button_return = TextButton(550, 310, 100, 50, green, bright_green, "Return", on_return_to_view_choose_challenge)
    buttons_challenging.append(button_return)
    # restart button
    button_restart = TextButton(550, 380, 100, 50, yellow, bright_yellow, "Restart", on_restart_challenge)
    buttons_challenging.append(button_restart)
    # quit button
    button_quit = TextButton(550, 450, 100, 50, red, bright_red, "Quit", on_quit)
    buttons_challenging.append(button_quit)


def show_challenging_buttons():
    for button in buttons_challenging:
        if button.name == "button_hint":
            if game_switches.auto_moving:
                button.param = True
            else:
                button.param = False
        button.blit(screen)


def check_challenging_buttons(event):
    # print(f"called check_challenging_buttons with event type{event.type}")
    if hasattr(event, "pos"):
        mouse_x, mouse_y = event.pos
    else:
        mouse_x, mouse_y = pygame.mouse.get_pos()
    button_to_operate = None
    for button in buttons_challenging:
        # print(f"button postion {button.get_rect()}, mouse position {mouse_x}, {mouse_y}")
        if button.get_rect().collidepoint(mouse_x, mouse_y):
            button_to_operate = button
            break
    else:
        return
    if event.type == pygame.MOUSEBUTTONDOWN:
        button_to_operate.on_mouse_down()
    elif event.type == pygame.MOUSEBUTTONUP:
        button_to_operate.on_mouse_up()


def register_challenge_complete_buttons():
    global buttons_challenge_complete
    # return button
    button_return = TextButton(550, 310, 100, 50, green, bright_green, "Return", on_return_to_view_choose_challenge)
    buttons_challenge_complete.append(button_return)
    # restart button
    button_restart = TextButton(550, 380, 100, 50, yellow, bright_yellow, "Restart", on_restart_challenge)
    buttons_challenge_complete.append(button_restart)
    # quit button
    button_quit = TextButton(550, 450, 100, 50, red, bright_red, "Quit", on_quit)
    buttons_challenge_complete.append(button_quit)


def show_challenge_complete_buttons():
    for button in buttons_challenge_complete:
        button.blit(screen)


def check_challenge_complete_buttons(event):
    # print(f"called check_challenge_complete_buttons with event type{event.type}")
    if hasattr(event, "pos"):
        mouse_x, mouse_y = event.pos
    else:
        mouse_x, mouse_y = pygame.mouse.get_pos()
    button_to_operate = None
    for button in buttons_challenge_complete:
        # print(f"button postion {button.get_rect()}, mouse position {mouse_x}, {mouse_y}")
        if button.get_rect().collidepoint(mouse_x, mouse_y):
            button_to_operate = button
            break
    else:
        return
    if event.type == pygame.MOUSEBUTTONDOWN:
        button_to_operate.on_mouse_down()
    elif event.type == pygame.MOUSEBUTTONUP:
        button_to_operate.on_mouse_up()


# initial
register_global_buttons()

# board outline 外框
challenge_area_width = 540
challenge_area_height = 540
rect_boardOutline = pygame.Rect(0, 0, challenge_area_width, challenge_area_height)
outer_edge_width = 30


def blit_challenge_area():
    pygame.draw.rect(screen, white, rect_boardOutline)


# interactive board area (Surface) 停车场部分（Surface）
board_area = pygame.Surface((480, 480))


def blit_board_area():
    board_area.fill(white)
    blit_lot()
    blit_spots()
    blit_cars()
    screen.blit(board_area, (outer_edge_width, outer_edge_width))


# draws the parking lot on the board area (Rect) 在停车场上画停车位（Rect）
lot = pygame.Rect(0, 0, 480, 480)


def blit_lot():
    pygame.draw.rect(board_area, black, lot, 1)
    exit_cover = pygame.draw.line(board_area, white, (480, 160), (480, 240), 8)


def blit_spots():
    for i in range(6):
        for j in range(6):
            spot = pygame.Rect(20 + 80 * i, 20 + 80 * j, 40, 40)
            pygame.draw.rect(board_area, black, spot, 3)


# car for test
car1Img = pygame.image.load(f"{directory}/img/car1.png")  # returns a surface
# car1 = Car("X", 2, 0, 160, "h", car1Img)
#
# car2Img = pygame.image.load("img/car1.png")  # returns a surface
# car2 = Car("A", 2, 400, 160, "v", car2Img)
# print(car2.collider.x)
# print(car2.collider.y)
# print(car2.collider)

challenges = {}
cars = {}


# load all the challenges from file
# deprecated
# def load_challenges():
#     with open("challenge/challenges.txt", "rt") as f:
#         lines = f.readlines()
#         for i in range(len(lines)):
#             if lines[i][0] == "#":
#                 temp = lines[i].split()
#                 challenge_alias = temp[1]
#                 challenges[challenge_alias] = {}
#                 temp = lines[i + 1].split()
#                 n_cars = int(temp[0])
#                 n_moves_sol = int(temp[1])
#                 cars = {}
#                 for j in range(1, n_cars + 1):
#                     attr = lines[i + 1 + j].split()
#                     car_alias = attr[0]
#                     cars[car_alias] = {}
#                     cars[car_alias]["length"] = int(attr[1])
#                     cars[car_alias]["j"] = int(attr[2])
#                     cars[car_alias]["i"] = int(attr[3])
#                     cars[car_alias]["direction"] = attr[4]
#                 challenges[challenge_alias]["config"] = cars
#                 challenges[challenge_alias]["n_moves_sol"] = n_moves_sol


def load_challenges():
    global challenges
    challenges = Common.load_challenges()


load_challenges()
print(challenges)
load_user_data()
pprint.pprint(user_data)


selected_challenge_alias = ""
def on_challenge_selected(selected_challenge):
    global selected_challenge_alias
    selected_challenge_alias = selected_challenge


def view_choose_challenge():
    global challenges, challenge_alias, names
    blit_background()
    font = pygame.font.Font("freesansbold.ttf", 48)
    # font = pygame.font.SysFont("Arial", 96, True)
    title = font.render("Choose a Challenge to Start", True, white)
    title_x = (globalWidth - title.get_width()) / 2
    title_y = 50
    screen.blit(title, (int(title_x), int(title_y)))

    font = pygame.font.SysFont("comicsansms", 20, True)
    desc_be = font.render("Beginner", True, white)
    desc_y = 50+48+10
    desc_be_x = 20
    screen.blit(desc_be, (int(desc_be_x), int(desc_y)))
    screen.blit(icon_difficulty_beginner, (desc_be_x + desc_be.get_width() + 10, desc_y))

    desc_in = font.render("Intermediate", True, white)
    desc_in_x = 20 + desc_be.get_width() + 10 + 35 + 20
    screen.blit(desc_in, (int(desc_in_x), int(desc_y)))
    screen.blit(icon_difficulty_intermediate, (desc_in_x + desc_in.get_width() + 10, desc_y))

    desc_ad = font.render("Advanced", True, white)
    desc_ad_x = 20 + desc_in_x + desc_in.get_width() + 10 + 35 + 20
    screen.blit(desc_ad, (int(desc_ad_x), int(desc_y)))
    screen.blit(icon_difficulty_advanced, (desc_ad_x + desc_ad.get_width() + 10, desc_y))

    desc_ex = font.render("Expert", True, white)
    desc_ex_x = 20 + desc_ad_x + desc_ad.get_width() + 10 + 35 + 20
    screen.blit(desc_ex, (int(desc_ex_x), int(desc_y)))
    screen.blit(icon_difficulty_expert, (desc_ex_x + desc_ex.get_width() + 10, desc_y))

    desc_gm = font.render("Grand Master", True, white)
    desc_gm_x = 20 + desc_ex_x + desc_ex.get_width() + 10 + 70 + 20
    screen.blit(desc_gm, (int(desc_gm_x), int(desc_y)))
    screen.blit(icon_difficulty_grand_master, (desc_gm_x + desc_gm.get_width() + 10, desc_y))



    # font = pygame.font.Font("freesansbold.ttf", 32)
    font = pygame.font.SysFont("Arial", 32, True)
    selected = font.render("Selected challenge: {}".format(selected_challenge_alias), True, white)
    selected_x = (globalWidth - selected.get_width()) / 2
    selected_y = 550+30
    # screen.blit(selected, (int(selected_x), int(selected_y)))
    screen.blit(render("Selected challenge: {}".format(selected_challenge_alias), pygame.font.SysFont("Arial", 32, True), white, black, 2),
                (int(selected_x), int(selected_y)))

    # blits challenges available
    i = 0
    j = 0
    for k in challenges.keys():
        if i > 9:
            i = 1
            j += 1
        else:
            i += 1
        challenge_button(k, 30+230*j, 130+40*i, 60, 30, white, red, k, on_challenge_selected)
        difficulty_icon = names[f"icon_difficulty_{challenges[k]['difficulty']}"]
        screen.blit(difficulty_icon, (30+230*j+60+10, 130+40*i))


def show_challenge_alias():
    # font = pygame.font.Font("freesansbold.ttf", 24)
    # font = pygame.font.SysFont("Arial", 30, True)
    # # font = pygame.font.SysFont("comicsansms", 30, True)
    # title = font.render("Challenge {}".format(selected_challenge_alias), True, white)
    title_x = rect_boardOutline.width + 20
    title_y = 10
    # screen.blit(title, (int(title_x), int(title_y)))
    screen.blit(render("CHALLENGE {}".format(selected_challenge_alias), pygame.font.SysFont("comicsansms", 36, True), pygame.Color(('dodgerblue')), black, 3), (int(title_x), int(title_y)))


def show_best_history():
    # font = pygame.font.Font("freesansbold.ttf", 24)
    # font = pygame.font.SysFont("Arial", 24, True)
    # font = pygame.font.SysFont("comicsansms", 30, True)
    # title = font.render("Your Best Score: {}".format(selected_challenge_alias), True, white)
    title_x = rect_boardOutline.width + 20
    title_y = 70
    # screen.blit(title, (int(title_x), int(title_y)))
    screen.blit(render("Your Best Score: {} with {} moves".
                       format(user_data[username]["challenges_record"][selected_challenge_alias]["best_score"],
                              user_data[username]["challenges_record"][selected_challenge_alias]["min_moves_taken"])
                       , pygame.font.SysFont("freesansbold", 30, True), pygame.Color(('springgreen3')), black, 2),
                (int(title_x), int(title_y)))

def show_n_moves_taken():
    # font = pygame.font.Font("freesansbold.ttf", 24)
    # font = pygame.font.SysFont("Arial", 24, True)
    # font = pygame.font.SysFont("comicsansms", 30, True)
    # title = font.render("# moves taken:", True, white)
    title_x = rect_boardOutline.width + 20
    title_y = 100
    # screen.blit(title, (int(title_x), int(title_y)))
    screen.blit(render("# moves taken:", pygame.font.SysFont("Arial", 24, True), white, black, 2),
                (int(title_x), int(title_y)))
    button(str(n_moves_taken), rect_boardOutline.width + 20, 130, 60, 30, white, white)


def show_score():
    # font = pygame.font.Font("freesansbold.ttf", 24)
    # font = pygame.font.SysFont("Arial", 24, True)
    # font = pygame.font.SysFont("comicsansms", 30, True)
    # title = font.render("Score:", True, white)
    title_x = rect_boardOutline.width + 20
    title_y = 170
    # screen.blit(title, (int(title_x), int(title_y)))
    screen.blit(render("Score:", pygame.font.SysFont("Arial", 24, True), white, black, 2),
                (int(title_x), int(title_y)))
    # button("{0:.2f}".format(score), rect_boardOutline.width + 20, 200, 60, 30, white, white)
    button("{0:d}".format(score), rect_boardOutline.width + 20, 200, 60, 30, white, white)


def on_return_to_view_choose_challenge():
    print("Clicked return")
    global gameState
    game_switches.reset()
    gameState = GameState.MAIN
    blit_background()


def init_cars(config):
    global cars, target_car
    cars = {}
    for k, v in config.items():
        print(k)
        carImg = pygame.image.load("img/car_{}.png".format(k))  # returns a surface
        car = Car(k, int(v["length"]), Common.translate_j_to_x(v["j"]), Common.translate_i_to_y(v["i"]), v["direction"], carImg,
                  board_offset=outer_edge_width)
        cars[k] = car
    target_car = cars[next(iter(cars))]


def init_challenge(i):
    challenge = challenges[str(i)]
    init_cars(challenge["config"])


# 命令行选择关卡
# print("Challenges list:")
# challenge_list_string = ""
# for k in challenges.keys():
#     challenge_list_string += k + " "
# print(challenge_list_string)
# selected_challenge_alias = input("Please choose a challenge to begin:")
# selected_challenge_alias = "7"

# init_challenge(selected_challenge_alias)
# target_car = cars[next(iter(cars))]  # Moved into function init_challenge()
max_car_length = 3  # the possible max length of all cars. It can be dynamically calculated from the data of the each challenge but at present it is hard coded
n_moves_taken = 0  # the number of moves that the player has made
moves_history = []  # the history
score = 0


def blit_cars():
    for car in cars.values():
        board_area.blit(car.image, (car.collider.x, car.collider.y))
        # pygame.draw.rect(board_area, red, car.collider)  # Cover the cars images with red rectangles. 用红色矩形覆盖车辆图形。


def init_lot_state() -> list():
    lot_state = [["." for i in range(6)] for j in range(6)]
    for _ in range(max_car_length):
        lot_state[2].append(".")  # Give additional space for the row where the exit is located
    return lot_state


def get_current_lot_state() -> list:
    current_lot_state = init_lot_state()
    for car in cars.values():
        j = Common.translate_x_to_j(car.collider.x) - 1
        i = Common.translate_y_to_i(car.collider.y) - 1
        if car.direction == "H":
            for k in range(car.length):
                current_lot_state[i][j + k] = car.alias
        elif car.direction == "V":
            for k in range(car.length):
                current_lot_state[i + k][j] = car.alias
    return current_lot_state


def print_lot_state(lot_state: list) -> None:
    for i in range(len(lot_state)):
        print(lot_state[i])


def show_waiting():
    print("waiting")
    font = pygame.font.SysFont("comicsansms", 30, True)
    title = font.render("Waiting for Computation...", True, white)
    title_x = (board_area.get_width() - title.get_width()) / 2
    title_y = board_area.get_height() / 2 - 30 / 2
    # screen.blit(title, (int(title_x), int(title_y)))
    board_area.blit(render("Waiting for Computation...", pygame.font.SysFont("comicsansms", 30, True), bright_red, black, 2), (int(title_x), int(title_y)))
    screen.blit(board_area, (outer_edge_width, outer_edge_width))
    pygame.display.update()


def get_hint(curr_state, cars, target_car):
    solution, min_n_step, _ = rushhour.search.bfs(curr_state, cars, target_car)
    moves = list()
    for i in range(1, len(solution), 2):
        moves.append(solution[i])
    return moves


moves = list()
auto_move_generator = None
def on_hint(stop = False):
    if stop:
        print("on hint stopping")
    else:
        print("on_hint")
    global game_switches, moves, auto_move_generator
    if not stop:
        show_waiting()
        curr_state = get_current_lot_state()
        moves = get_hint(curr_state, cars, target_car)
        moves.pop()
        game_switches.auto_moving = True
        auto_move_generator = auto_move(moves)
    else:
        game_switches.auto_move_stopping = True


def auto_move(moves):
    global n_moves_taken, auto_move_generator
    for move in moves:
        car = cars[move[0]]
        origin_x = car.collider.x
        origin_y = car.collider.y
        car_i = Common.translate_y_to_i(origin_y)
        car_j = Common.translate_x_to_j(origin_x)
        if move[1] == "H":
            car_j += move[2]
            dest_x = Common.translate_j_to_x(car_j)
            dx = abs(dest_x - origin_x)
            inter_x = interpolation_x(origin_x, dest_x)
            for x in inter_x:
                car.collider.x = x
                yield
        else:
            car_i += move[2]
            dest_y = Common.translate_i_to_y(car_i)
            dy = abs(dest_y - origin_y)
            inter_y = interpolation_y(origin_y, dest_y)
            for y in inter_y:
                car.collider.y = y
                yield
        n_moves_taken += 1
        if game_switches.auto_move_stopping:
            break
    game_switches.auto_moving = False
    game_switches.auto_move_stopping = False
    moves.clear()
    auto_move_generator = None
    # Considering moving these four lines above to the main while loop when StopIteration is catched.


def interpolation_x(sink_x, dest_x):
    x = sink_x
    if sink_x > dest_x:
        # move left
        for i in range(1, sink_x - dest_x + 1, 1):
            x += -1
            yield x
    else:
        # move right
        for i in range(1, dest_x - sink_x + 1, 1):
            # print(i)
            x += 1
            yield x


def interpolation_y(sink_y, dest_y):
    y = sink_y
    if sink_y > dest_y:
        # move up
        for i in range(1, sink_y - dest_y + 1, 1):
            y += -1
            yield y
    else:
        # move right
        for i in range(1, dest_y - sink_y + 1, 1):
            y += 1
            yield y


def reset_challenge():
    global dragging_car, rectangle_dragging, n_moves_taken, moves_history, score
    dragging_car = ""
    rectangle_dragging = False
    n_moves_taken = 0
    moves_history = []
    moves_history.append(get_current_lot_state())
    score = 0


def on_restart_challenge():
    # game_switches.reset()
    # init_challenge(selected_challenge_alias)
    on_start_challenge()


def on_complete_challenge():
    global gameState, score
    gameState = GameState.CHALLENGE_COMPLETE
    register_challenge_complete_buttons()

    blit_background()
    blit_challenge_area()
    blit_board_area()
    print("Challenge {} complete".format(selected_challenge_alias))
    # score = (1 - 1 / n_moves_sol * (n_moves_taken - n_moves-sol)) * 100
    score = 100 * (1 - (n_moves_taken - int(challenges[selected_challenge_alias]["n_moves_sol"])) / int(challenges[selected_challenge_alias]["n_moves_sol"]))
    if score < 0:
        score = 0
    score = math.floor(score)
    print(score)
    user_data[username]["challenges_record"][selected_challenge_alias]["best_score"] = max(score, user_data[username]["challenges_record"][selected_challenge_alias]["best_score"])
    user_history_min_moves_taken = user_data[username]["challenges_record"][selected_challenge_alias]["min_moves_taken"]
    if user_history_min_moves_taken == "N/A":
        user_data[username]["challenges_record"][selected_challenge_alias]["min_moves_taken"] = n_moves_taken
    else:
        user_data[username]["challenges_record"][selected_challenge_alias]["min_moves_taken"] = min(n_moves_taken, user_history_min_moves_taken)
    write_user_data()
    sound_hooray.play()


def on_quit():
    # pass
    global running
    running = False
    quit()


rectangle_dragging = False
dragging_car = ""
offset_x = 0
offset_y = 0
running = True
while running:
    # show_global_buttons()
    if gameState == GameState.MAIN:
        view_choose_challenge()
        show_global_buttons()
        # word_box = pygame_functions.makeTextBox(300, 450, 150, 0, "", 3, 22)
        # pygame_functions.showTextBox(word_box)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                quit()
            elif event.type in [pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP]:
                check_global_buttons(event)
        if selected_challenge_alias != "":
            button("GO!", 150, 580, 100, 50, green, bright_green, on_start_challenge)
            if gameState == GameState.MAIN:
                button("Quit", 750, 580, 100, 50, red, bright_red, on_quit)
    elif gameState == GameState.CHALLENGING:
        show_challenging_buttons()
        show_global_buttons()
        # button("Return", 550, 310, 100, 50, green, bright_green, on_return_to_view_choose_challenge)
        if gameState == GameState.CHALLENGING:
            # button("Hint", 550, 240, 100, 50, green, bright_green, on_hint)
            # print(game_switches.auto_moving)
            if game_switches.auto_moving:
                if auto_move_generator is not None:
                    try:
                        auto_move_generator.__next__()
                    except StopIteration as e:
                        print("StopIteration happened")
                        print(e)
            # button("Restart", 550, 380, 100, 50, yellow, bright_yellow, on_restart_challenge)
            # button("Quit", 550, 450, 100, 50, red, bright_red, on_quit)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    quit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        check_global_buttons(event)
                        check_challenging_buttons(event)
                        mouse_x, mouse_y = event.pos
                        mouse_x -= outer_edge_width  # Translate to its relative coordinate against board_area 换算成与board_area的相对位置
                        mouse_y -= outer_edge_width
                        for car in cars.values():
                            if car.collider.collidepoint((mouse_x, mouse_y)):
                                # print(event.pos)
                                # print(car.collider)
                                dragging_car = car.alias
                                print("Clicked on car " + car.alias)
                                rectangle_dragging = True
                                # mouse_x, mouse_y = event.pos
                                offset_x = car.collider.x - mouse_x
                                offset_y = car.collider.y - mouse_y
                                break
                elif event.type == pygame.MOUSEBUTTONUP:  # Ends dragging
                    if event.button == 1:
                        rectangle_dragging = False
                        check_global_buttons(event)
                        check_challenging_buttons(event)
                        if dragging_car != "":
                            target = cars[dragging_car]
                            # Upon dragging ending, move the dragged car to the nearest spot depending on its current position 拖拽释放时，将车卡到最近的一个槽里
                            if target.direction == "H":
                                for i in range(5):
                                    if -40 + i * 80 < target.collider.x < 40 + i * 80:
                                        target.collider.x = i * 80
                                        break
                            elif target.direction == "V":
                                for i in range(5):
                                    if -40 + i * 80 < target.collider.y < 40 + i * 80:
                                        target.collider.y = i * 80
                                        break
                            cls = get_current_lot_state()
                            print_lot_state(cls)
                            if cls != moves_history[len(moves_history) - 1]:
                                moves_history.append(cls)
                                n_moves_taken += 1
                elif event.type == pygame.MOUSEMOTION:  # Move the car being dragged
                    if rectangle_dragging:
                        mouse_x, mouse_y = event.pos
                        mouse_x -= outer_edge_width
                        mouse_y -= outer_edge_width
                        # print(mouse_x)
                        target = cars[dragging_car]
                        colliders = [v.collider for k, v in cars.items() if k != dragging_car]
                        if target.direction == "H":
                            prevx = target.collider.x
                            target.collider.x = mouse_x + offset_x
                            if target.collider.collidelist(colliders) > -1 or (not lot.contains(target.collider) and target.collider.y != Common.translate_i_to_y(3) or (target.collider.y == Common.translate_i_to_y(3) and target.collider.x < 0)):
                                target.collider.x = prevx
                                offset_x = target.collider.x - mouse_x  # For avoiding the dragged car from flashing over another car when the cursor moves over another car. 为了防止闪现跨过车辆。
                        elif target.direction == "V":
                            prevy = target.collider.y
                            target.collider.y = mouse_y + offset_y
                            if target.collider.collidelist(colliders) > -1 or not lot.contains(target.collider):
                                target.collider.y = prevy
                                offset_y = target.collider.y - mouse_y  # For avoid the dragged car from flashing over another car when the cursor moves over another car. 为了防止闪现跨过车辆。

                        # print("{} {}".format(target_car.collider.x, challenge_area_width - 2 * outer_edge_width))
                        if target_car.collider.x > challenge_area_width - 2 * outer_edge_width:
                            cls = get_current_lot_state()
                            print_lot_state(cls)
                            if cls != moves_history[len(moves_history) - 1]:
                                moves_history.append(cls)
                                n_moves_taken += 1
                            on_complete_challenge()
                    # else:
                    #     check_challenging_buttons(event)
                elif event.type == pygame.KEYDOWN:  # for testing
                    if event.key == pygame.K_p:
                        cls = get_current_lot_state()
                        print_lot_state(cls)
                # blit_board_outline()
            blit_challenge_area()
            blit_board_area()
            show_challenge_alias()
            show_best_history()
            show_n_moves_taken()

            # update cars position
            # blit_cars()

    elif gameState == GameState.CHALLENGE_COMPLETE:
        show_challenge_complete_buttons()
        show_global_buttons()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                quit()
            elif event.type in [pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP]:
                check_global_buttons(event)
                check_challenge_complete_buttons(event)
        # screen.fill(black)
        # button("Return", 550, 310, 100, 50, green, bright_green, on_return_to_view_choose_challenge)
        if gameState == GameState.CHALLENGE_COMPLETE:
            # button("Restart", 550, 380, 100, 50, yellow, bright_yellow, on_restart_challenge)
            # button("Quit", 550, 450, 100, 50, red, bright_red, on_quit)
            show_challenge_alias()
            show_best_history()
            show_n_moves_taken()
            show_score()

    # update
    pygame.display.update()
    # clock ticks
    fpsClock.tick(60)

input("Please enter any key to exit")

if __name__ == "__main__":
    # first_screen()
    pass
